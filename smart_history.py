#!/usr/bin/env python

wait = 60*60*24
#wait = 3
disk_names = ["/dev/sda", "/dev/sdb", "/dev/sdc"]
nrs_attribs = [1, 3, 4, 5, 7, 9, 10, 12, 183, 184, 187, 188, 189,
               190, 191, 192, 193, 194, 195, 197, 198, 199, 240, 241, 242]

finish = False


def main():
    print("TEST")
    from pySMART import Device
    import time
    import signal
    import pprint
    import yaml
    pp = pprint.PrettyPrinter(indent=4)
    mprint = pp.pprint
    smart_disks = [Device(disk_name) for disk_name in disk_names]

    def int_handler(signum, frame):
        print('Signal handler called with signal', signum)
        print("Closing program")
        global finish
        finish = True

    signal.signal(signal.SIGTERM, int_handler)
    signal.signal(signal.SIGINT, int_handler)
    try:
        with open("/var/log/smart_history.yaml", "r") as my_file:
            data = yaml.safe_load(my_file)
        print("Appending on top of history file: smart_history.yaml")
    except FileNotFoundError:
        print("Creating new history file: smart_history.yaml")
        data = []

    while not finish:
        print("Looping")
        entry = {}
        entry["h_time"] = time.strftime("%Y_%m_%d %H:%M:%S")
        entry["time"] = time.time()
        for disk_name, smart_disk in zip(disk_names, smart_disks):
            entry[disk_name] = {}
            entry[disk_name]["assessment"] = smart_disk.assessment
            for nr_attrib in nrs_attribs:
                attrib = smart_disk.attributes[nr_attrib]
                entry[disk_name][nr_attrib] = [
                    attrib.name, attrib.raw, attrib.when_failed]
        data.append(entry)
        with open("/var/log/smart_history.yaml", "w+") as my_file:
            yaml.dump(data, my_file)
        if not finish:
            time.sleep(wait)


if __name__ == "__main__":
    main()
