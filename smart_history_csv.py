#!/usr/bin/env python

disk_names = ["/dev/sda", "/dev/sdb", "/dev/sdc"]
nrs_attribs = [1, 3, 4, 5, 7, 9, 10, 12, 183, 184, 187, 188, 189,
               190, 191, 192, 193, 194, 195, 197, 198, 199, 240, 241, 242]
data = []

finish = False


def main():
    import argparse
    import pprint
    import yaml
    import csv

    pp = pprint.PrettyPrinter(indent=4)
    mprint = pp.pprint

    parser = argparse.ArgumentParser(
        description="Convert smart smart history to csv")
    parser.add_argument("--disk", dest="disk", type=str,
                        help="Disk name for data filtering")
    parser.add_argument("--outfn", dest="outfn", type=str,
                        default="smart_history.csv",
                        help="Output csv filename")
    parser.add_argument("--in", dest="in", type=str,
                        default="smart_history.yaml",
                        help="Input smart monitor history yaml filename")
    parser.add_argument("nrs_attribs", nargs="*", default=nrs_attribs,
                        type=int,
                        help="List of attribute numbers for filterin")
    args = parser.parse_args()
    print(args.disk)
    print(args.nrs_attribs)

    with open("/var/log/smart_history.yaml", "r") as my_file:
        data = yaml.safe_load(my_file)

    mprint(data)

    rows = []
    with open(args.outfn, "w+", newline='') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=' ')
        # getting first row names for csv column names
        first_row = ["time"]
        disk_dict = data[0][args.disk]
        for nr_attrib in args.nrs_attribs:
            first_row.append(disk_dict[nr_attrib][0])
        print("First Row:")
        mprint(first_row)
        print()
        csvwriter.writerow(first_row)

        # Rest of rows (with data)
        for entry in data:
            row = []
            print("Entry: ", entry["h_time"])
            disk_dict = entry[args.disk]
            row.append(entry["time"])
            for nr_attrib in args.nrs_attribs:
                row.append(disk_dict[nr_attrib][1])
            print(row)
            csvwriter.writerow(row)
            rows.append(row)


if __name__ == "__main__":
    main()
