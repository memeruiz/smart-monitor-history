#!/usr/bin/env python

plot_cols = [1, 3, 4, 6, 7, 8, 12, 13, 14]


def main():
    import argparse
    import pprint
    import yaml
    import csv
    import matplotlib as mpl
    from mpl_toolkits.axes_grid1 import host_subplot
    import mpl_toolkits.axisartist as AA
    import matplotlib.pyplot as plt
    import numpy as np

    pp = pprint.PrettyPrinter(indent=4)
    mprint = pp.pprint

    parser = argparse.ArgumentParser(
        description="Convert smart smart history to csv")
    parser.add_argument("--csv_file", dest="csv_file", type=str,
                        default="smart_history.csv",
                        help="Input smart monitor history csv filename")
    parser.add_argument("plot_cols", nargs="*", default=plot_cols,
                        type=int,
                        help="List of columns to plot (col 0 is always ""time"")")
    args = parser.parse_args()

    rows = []
    first = True
    with open(args.csv_file, "r", newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=' ')
        for row in csvreader:
            print(row)
            if first:
                first_row = row
                first = False
            else:
                rows.append(row)

    print("First row")
    print(first_row)
    print("Rows:")
    print(rows)

    #fig, ax = plt.subplots()
    host = host_subplot(111, axes_class=AA.Axes)
    nprows = np.array(rows).astype(float)
    #ax.set_xlabel('time (s)')
    host.set_xlabel('time (s)')
    # time (on x axis)
    t = nprows[:, 0]
    t -= np.array([t[0]]*len(t))

    # other data
    ys = []
    first = True
    offset = 0
    offset_inc = 60
    for col in args.plot_cols:
        ys.append(nprows[:, col])
        y = nprows[:, col]
        if first:
            first = False
            host.set_ylabel(first_row[1])
            p, = host.plot(t, y, label=first_row[1])
            host.axis["left"].label.set_color(p.get_color())
        else:
            nx = host.twinx()
            nx.set_ylabel(first_row[col])
            new_fixed_axis = nx.get_grid_helper().new_fixed_axis
            nx.axis["right"] = new_fixed_axis(
                loc="right",
                axes=nx, offset=(offset, 0))
            offset += offset_inc
            p, = nx.plot(t, y, label=first_row[col])
            nx.axis["right"].label.set_color(p.get_color())
    print("Cols to plot as Ys")
    print(ys)

    #ax.plot(t, ys[0])
    #bx = ax.twinx()
    #bx.spines['right'].set_position(('axes', 1.0))
    #cx = ax.twinx()
    #cx.spines['right'].set_position(('axes', 1.15))
    #bx.plot(t, ys[1])
    #cx.plot(t, ys[2])
    adj = (len(args.plot_cols)-1)*0.06
    print(adj)
    # fig.subplots_adjust(right=1.0-adj)
    # fig.legend()
    # plt.subplots_adjust(right=1.0-adj)
    plt.subplot_tool()
    host.legend()
    plt.show()


if __name__ == "__main__":
    main()
