# Smart Monitor History

Smart Monitor History

## Description

Records to a YAML file a regular state of S.M.A.R.T attributes from a list of disks.

There is a utility program to convert this data to a csv file to be able to import on your spreadsheet for plotting.

The idea is to use this plotting to visualice possible disk failures.

## Usage
- Edit disk_names, nrs_attribs and wait on smart\_history.py daemon file
- Run smart_history.py:

```smart_history.py```

This will create or expand this file:

```smart_history.yaml```

- Run the csv converter:

```
./smart_history_csv.py --disk /dev/sda --outfn smart_history.csv 1 4 5 7 12 183 184 187 189 191 193 195 197 198 199
```

This allows you to select the disks and attributes to anaylize

- Run the plotter:

```
plot_csv.py
```

## Roadmap

Modify the daemon to allow it to run from systemd

## Authors and acknowledgment
Federico Ruiz Ugalde

## License
GPLv3
